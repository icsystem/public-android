package com.ics.emergencybutton.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by angga on 10/22/16.
 */

public class CachedManager {
    private SharedPreferences sharedPreferences;

    public CachedManager(Context context){
        this.sharedPreferences = context.getSharedPreferences("cache mangaer",Context.MODE_PRIVATE);
    }

    public void savePhoneNumber(String phoneNumber){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("phone_number", phoneNumber);
        editor.apply();
    }

    public String getPhoneNumber(){
        return sharedPreferences.getString("phone_number","");
    }

    //TODO change from API
    public void saveIsVerified(boolean isVerified){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Log.d("ctr", "saveIsVerified: " + isVerified);
        editor.putBoolean("is_verified", isVerified);
        editor.apply();
    }

    public boolean getIsVerified(){
        return sharedPreferences.getBoolean("is_verified",false);
    }

    public void saveFCMToken(String fcmToken){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("fcm_token",fcmToken);
        editor.apply();
    }

    public String getFCMToken(){
        return sharedPreferences.getString("fcm_token","");
    }

    public void clear(){
        sharedPreferences.edit().clear().apply();
    }
}
