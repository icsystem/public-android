package com.ics.emergencybutton.internal;

import android.content.Context;
import android.content.SharedPreferences;

import com.ics.emergencybutton.pojo.AccountInfo;

/**
 * Created by angga on 10/19/16.
 */

public class CachedAccountStore {
    private final String TAG = CachedAccountStore.class.getSimpleName();

    private final SharedPreferences sharedPreferences;

    public CachedAccountStore(Context context){
        this.sharedPreferences = context.getSharedPreferences(TAG,Context.MODE_PRIVATE);
    }

    public boolean containAccountInfo(){
        return sharedPreferences.contains("authentication_token") && sharedPreferences.contains("is_verified");
    }

    public void cachedAccountInfo(AccountInfo accountInfo){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("authentication_token",accountInfo.getAuthenticationToken());
        editor.putBoolean("is_verified",accountInfo.isVerified());
        editor.apply();
    }

    public AccountInfo getCachedAccountInfo(){
        if(!containAccountInfo()){
            throw new RuntimeException("There is no cached Account Info yet");
        }

        String authenticationToken = sharedPreferences.getString("authentication_token","");
        boolean isVerified = sharedPreferences.getBoolean("is_verified",false);

        return new AccountInfo(authenticationToken,isVerified);
    }

}
