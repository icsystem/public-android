package com.ics.emergencybutton;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
/**
 * Created by angga on 10/16/16.
 */

public class EmergencyButtonApplication extends Application {
    private static EmergencyButtonApplication instance;
    private EmergencyButtonComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        component = EmergencyButtonComponent.Initializer.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static EmergencyButtonApplication getInstance() {
        return instance;
    }


    public static EmergencyButtonComponent getComponent() {
        return instance.component;
    }
}
