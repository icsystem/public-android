package com.ics.emergencybutton;

/**
 * Created by angga on 10/16/16.
 */

import android.app.Application;

import com.ics.emergencybutton.module.ApplicationModule;
import com.ics.emergencybutton.module.ExternalModule;
import com.ics.emergencybutton.module.InternalModule;
import com.ics.emergencybutton.ui.activity.EmergencyDetailActivity;
import com.ics.emergencybutton.ui.activity.HomeActivity;
import com.ics.emergencybutton.ui.activity.LoginActivity;
import com.ics.emergencybutton.ui.activity.RegistrationActivity;
import com.ics.emergencybutton.ui.activity.VerificationCodeActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                InternalModule.class,
                ExternalModule.class
        }
)
public interface EmergencyButtonComponent {
    void inject(LoginActivity loginActivity);
    void inject(RegistrationActivity registrationActivity);
    void inject(VerificationCodeActivity verificationCodeActivity);
    void inject(HomeActivity homeActivity);
    void inject(EmergencyDetailActivity emergencyDetailActivity);

    final class Initializer {
                public static EmergencyButtonComponent init(Application application){
                        return DaggerEmergencyButtonComponent.builder()
                                        .applicationModule(new ApplicationModule(application))
                                        .build();
            }
    }
}
