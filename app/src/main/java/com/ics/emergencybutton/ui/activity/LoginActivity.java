package com.ics.emergencybutton.ui.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ics.emergencybutton.BuildConfig;
import com.ics.emergencybutton.EmergencyButtonApplication;
import com.ics.emergencybutton.R;
import com.ics.emergencybutton.databinding.ActivityLoginBinding;
import com.ics.emergencybutton.internal.CachedAccountStore;
import com.ics.emergencybutton.internal.CachedManager;
import com.ics.emergencybutton.network.EmergencyButtonService;
import com.ics.emergencybutton.pojo.AccountInfo;
import com.ics.emergencybutton.util.ConnectionDetector;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity {

    private String TAG = LoginActivity.class.getSimpleName();

    private ActivityLoginBinding binding;
    private ProgressDialog progressDialog;
    private ConnectionDetector connectionDetector;

    @Inject
    EmergencyButtonService emergencyButtonService;

    @Inject
    CachedAccountStore cachedAccountStore;

    @Inject
    CachedManager cachedManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EmergencyButtonApplication.getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        initViews();
        requestPermittion();

    }

    private void initViews(){
            //TODO adding condition is verified
        LocalBroadcastManager.getInstance(this).registerReceiver(tokenReceiver,
                new IntentFilter("tokenReceiver"));
        if(cachedAccountStore.containAccountInfo() && cachedManager.getIsVerified()){
            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
            finish();
        }

        if(cachedAccountStore.containAccountInfo() && !cachedManager.getIsVerified()){
            startActivity(new Intent(LoginActivity.this, VerificationCodeActivity.class));
            finish();
        }

        connectionDetector = new ConnectionDetector(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon tunggu ... ");

        binding.daftarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
            }
        });

        binding.masukBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = binding.userInput.getText().toString().trim();
                String password = binding.passwordInput.getText().toString().trim();
                boolean hasError = false;

                if(!connectionDetector.isConnectingToInternet()){
                    Toast.makeText(LoginActivity.this, "Mohon cek koneksi internet", Toast.LENGTH_SHORT).show();
                }else if(phoneNumber.isEmpty() && password.isEmpty()){
                    Toast.makeText(LoginActivity.this, "No telp dan Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    hasError = true;
                }else if(phoneNumber.isEmpty()){
                    Toast.makeText(LoginActivity.this, "No telp tidak boleh ksoong", Toast.LENGTH_SHORT).show();
                    hasError = true;
                }else if(password.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Password tidak boleh ksoong", Toast.LENGTH_SHORT).show();
                    hasError = true;
                }

                if(!hasError){

                    Log.d(TAG, "onClick: ctr " + phoneNumber + "passwrd " + password );
                    emergencyButtonService.login(phoneNumber,password)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<AccountInfo>() {
                                @Override
                                public void onStart() {
                                    super.onStart();
                                    progressDialog.show();
                                }

                                @Override
                                public void onCompleted() {
                                    progressDialog.dismiss();
                                    startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                                    finish();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d(TAG, "onError: ctr " + e.getMessage());
                                    if(e.getMessage().contains("HTTP 500 Internal Server Error")){
                                        Toast.makeText(LoginActivity.this, "Mohon cek No telp dan Password ", Toast.LENGTH_SHORT).show();
                                    }
                                    e.printStackTrace();
                                    progressDialog.dismiss();
                                }

                                @Override
                                public void onNext(AccountInfo accountInfo) {
                                    Log.d(TAG, "onNext: ctr " + accountInfo.getAuthenticationToken());
                                    cachedAccountStore.cachedAccountInfo(accountInfo);
                                }
                            });
                }
            }
        });

        if (BuildConfig.DEBUG) {
            binding.textViewTitle.setOnClickListener(new View.OnClickListener() {
                private int clickCount = 0;

                @Override
                public void onClick(View v) {
                    clickCount++;
                    if (clickCount == 4) {
                        clickCount = 0;

                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setMessage("Do you want to access developer setting page?");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(LoginActivity.this, DeveloperPreferenceActivity.class));
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.setCancelable(false);

                        builder.create().show();
                    }
                }
            });
        }
    }

    BroadcastReceiver tokenReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String token = intent.getStringExtra("token");
            if(token != null)
            {
                Log.d("ctr", "onReceive: ctr " + token);
                cachedManager.saveFCMToken(token);
            }
        }
    };

    private void requestPermittion(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(LoginActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.DISABLE_KEYGUARD, Manifest.permission.INTERNET, Manifest.permission.VIBRATE, Manifest.permission.WAKE_LOCK, Manifest.permission.RECEIVE_BOOT_COMPLETED},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

}
