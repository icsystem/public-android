package com.ics.emergencybutton.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.ImageFormat;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ics.emergencybutton.EmergencyButtonApplication;
import com.ics.emergencybutton.R;
import com.ics.emergencybutton.databinding.ActivityVerificationCodeBinding;
import com.ics.emergencybutton.internal.CachedAccountStore;
import com.ics.emergencybutton.internal.CachedManager;
import com.ics.emergencybutton.network.EmergencyButtonService;
import com.ics.emergencybutton.pojo.AccountInfo;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class VerificationCodeActivity extends Activity {

    private static final String TAG = VerificationCodeActivity.class.getSimpleName();
    private static String KEY_PHONE_NUMBER = "phone_number";

    private ActivityVerificationCodeBinding binding;
    private CountDownTimer cTimer = null;
    private final String FORMAT = "%02d:%02d";

    @Inject EmergencyButtonService emergencyButtonService;
    @Inject CachedManager cachedManager;
    @Inject CachedAccountStore cachedAccountStore;

    public static Intent generateVerification(Context context){
        return new Intent(context,VerificationCodeActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        EmergencyButtonApplication.getComponent().inject(this);
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_verification_code);
        startTimer();
        initViews();
    }

    private void initViews(){

        final String phoneNumber = String.valueOf(cachedManager.getPhoneNumber());

        binding.noHpText.setText(phoneNumber);
        binding.btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String codeVerification = binding.verificationCodeInput.getText().toString();
                boolean hasError = false;
                if(codeVerification.isEmpty()){
                    Toast.makeText(VerificationCodeActivity.this, "Mohon diisi kode verifikasi", Toast.LENGTH_SHORT).show();
                    hasError = true;
                }
                if(!hasError){
                    final ProgressDialog progressDialog = new ProgressDialog(VerificationCodeActivity.this);
                    progressDialog.setMessage("Mohon tunggu ... ");
                    progressDialog.show();

                    emergencyButtonService.verification(codeVerification)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<AccountInfo>() {
                                @Override
                                public void call(AccountInfo accountInfo) {
                                    progressDialog.dismiss();
                                    cachedAccountStore.cachedAccountInfo(accountInfo);
                                    cachedManager.saveIsVerified(true);
                                    startActivity(new Intent(VerificationCodeActivity.this,HomeActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    finish();
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    progressDialog.dismiss();
                                    Toast.makeText(VerificationCodeActivity.this, "Mohon di cek kembali kode verifikasi", Toast.LENGTH_SHORT).show();
                                    throwable.printStackTrace();
                                    Log.d(TAG, "call: ctr " + throwable.getMessage());
                                }
                            });
                }

            }
        });
        binding.btnGantiNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText noHP = new EditText(VerificationCodeActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                noHP.setLayoutParams(lp);
                noHP.setHint("No Handphone");
                noHP.setInputType(InputType.TYPE_CLASS_NUMBER);

                final AlertDialog dialog= new AlertDialog.Builder(VerificationCodeActivity.this)
                        .setTitle("Edit No Handphone")
                        .setView(noHP)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setNegativeButton("BATAL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

                dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Boolean wantToCloseDialog = false;

                        String phoneNumber = noHP.getText().toString().trim();
                        Log.d(TAG, "onClick: ctr current number " + phoneNumber);
                        if(phoneNumber.isEmpty()){
                            Toast.makeText(VerificationCodeActivity.this, "No Handphone tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            return;
                        }else{
                            editNumber(phoneNumber);
                            Toast.makeText(VerificationCodeActivity.this, "Mengirim kode", Toast.LENGTH_SHORT).show();
                            wantToCloseDialog = true;
                            cachedManager.savePhoneNumber(phoneNumber);

                        }

                        if(wantToCloseDialog){
                            dialog.dismiss();
                        }

                    }
                });
            }
        });

        binding.resendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = cachedManager.getPhoneNumber();
                resendCodeVerification(phoneNumber);
            }
        });
    }

    private void startTimer() {
        cTimer = new CountDownTimer(300000, 1000) {
            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                binding.timerText.setText("TIME : " + String.format(FORMAT, minutes,seconds));
                binding.resendText.setVisibility(View.GONE);

            }
            public void onFinish() {
                binding.timerText.setVisibility(View.GONE);
                binding.resendText.setVisibility(View.VISIBLE);
                cancelTimer();
            }
        };

        cTimer.start();
    }

    private void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }
    private void resendCodeVerification(String phoneNumber){
        emergencyButtonService.resendCode(phoneNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<AccountInfo>() {
                    @Override
                    public void call(AccountInfo accountInfo) {
                        Log.d(TAG, "call: ctr sending " + accountInfo.getMessages());
                        cancelTimer();
                        startTimer();
                        binding.timerText.setVisibility(View.VISIBLE);
                        binding.resendText.setVisibility(View.GONE);

                        cachedAccountStore.cachedAccountInfo(accountInfo);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.d(TAG, "call: " +  throwable.getMessage());
                        if(throwable.getMessage().contains("HTTP 422 Unprocessable Entity")){
                            Toast.makeText(VerificationCodeActivity.this, "Mohon ditunggu beberapa menit", Toast.LENGTH_SHORT).show();
                        }
                        throwable.printStackTrace();
                    }
                });
    }

    private void editNumber(final String phoneNumber){

        emergencyButtonService.editNumber(phoneNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<AccountInfo>() {
                    @Override
                    public void call(AccountInfo accountInfo) {
                        Log.d(TAG, "call: ctr sending edit number" + accountInfo.getMessages() + phoneNumber);
                        cancelTimer();
                        startTimer();
                        binding.timerText.setVisibility(View.VISIBLE);
                        binding.resendText.setVisibility(View.GONE);
                        binding.noHpText.setText(phoneNumber);
                        cachedManager.savePhoneNumber(phoneNumber);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.d(TAG, "call: " +  throwable.getMessage());
                        throwable.printStackTrace();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ctr edit no " + cachedManager.getPhoneNumber());
        binding.noHpText.setText(String.valueOf(cachedManager.getPhoneNumber()));
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Mohon untuk menyelesaikan proses verifikasi", Toast.LENGTH_SHORT).show();
    }
}
