package com.ics.emergencybutton.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ics.emergencybutton.EmergencyButtonApplication;
import com.ics.emergencybutton.R;
import com.ics.emergencybutton.databinding.ActivityEmergencyDetailBinding;
import com.ics.emergencybutton.network.EmergencyButtonService;
import com.ics.emergencybutton.pojo.OpenCase;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class EmergencyDetailActivity extends AppCompatActivity {

    private final String TAG = EmergencyDetailActivity.class.getSimpleName();
    private static String KEY_LATITUDE  = "latitude";
    private static String KEY_LONGITUDE  = "longitude";
    private static String KEY_CASE_TYPE = "case_type";

    private double latitude = 0;
    private double longitude = 0;
    private String caseType;
    private ActivityEmergencyDetailBinding binding;
    private OpenCase openCase;

    @Inject
    EmergencyButtonService emergencyButtonService;

    public static Intent generateEmergencyDetail(Context context, double latitude, double longitude, String caseType){

        Intent intent = new Intent(context,EmergencyDetailActivity.class);
        intent.putExtra(KEY_LATITUDE,latitude);
        intent.putExtra(KEY_LONGITUDE,longitude);
        intent.putExtra(KEY_CASE_TYPE,caseType);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EmergencyButtonApplication.getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_emergency_detail);
        initViews();

    }

    private void initViews(){

        latitude = getIntent().getDoubleExtra(KEY_LATITUDE,0);
        longitude = getIntent().getDoubleExtra(KEY_LONGITUDE,0);
        caseType = getIntent().getStringExtra(KEY_CASE_TYPE);
        Log.d(TAG, "onClick: ctr " + latitude + "longitude " + longitude + "case " + caseType);

        statusEmergency(caseType);

        binding.btnSafe.setOnClickListener(new View.OnClickListener() {
            private int count = 0;
            @Override
            public void onClick(View v) {
                count++;

                if(count == 1){
                    Toast.makeText(EmergencyDetailActivity.this, "Tekan " + count + " lagi untuk mengaktifkan", Toast.LENGTH_SHORT).show();
                }

                if(count == 2 ){
                    count = 0;
                    sendSafeCase();
                }
            }
        });
    }

    private void statusEmergency(final String caseType){

        if(caseType.equals(HomeActivity.CASE_TYPE.kejahatan.name())){
            binding.btnSos.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.btn_sos_crime));
        }else if(caseType.equals(HomeActivity.CASE_TYPE.bencana_kebakaran.name())){
            binding.btnSos.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.btn_sos_fire));
        }else if(caseType.equals(HomeActivity.CASE_TYPE.bantuan_medis.name())){
            binding.btnSos.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.btn_sos_health));
        }else if(caseType.equals(HomeActivity.CASE_TYPE.kecelakaan.name())){
            binding.btnSos.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.btn_sos_accident));
        }
        String  caseTypeTitle = caseType.replace("_"," ");
        binding.textCase.setText(caseTypeTitle);

        binding.btnSos.setOnClickListener(new View.OnClickListener() {
            private int count = 0;
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: ctr " + caseType);
                count++;

                if(count == 1){
                    Toast.makeText(EmergencyDetailActivity.this, "Tekan " + count + " lagi untuk mengaktifkan", Toast.LENGTH_SHORT).show();
                }

                if(count == 2 ){
                    count = 0;
                    sendEmergencyCase(latitude,longitude,caseType);

                }
            }
        });
    }

    private void sendEmergencyCase(double latitude, double longitude, String caseType){

        emergencyButtonService.sendEmergencyCase(latitude,longitude,caseType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<OpenCase>() {
                    @Override
                    public void call(OpenCase aVoid) {
                        Log.d(TAG, "call: ctr id emergecny" + aVoid.getId() + "case status " + aVoid.getCaseStatus());
                        openCase = aVoid;
                        binding.btnSos.setVisibility(View.GONE);
                        binding.textAdditionSos.setVisibility(View.GONE);
                        binding.textCase.setVisibility(View.GONE);
                        binding.btnSafe.setVisibility(View.VISIBLE);
                        binding.textAdditionSafe.setVisibility(View.VISIBLE);

                    }
                });
    }

    private void sendSafeCase(){

        emergencyButtonService.sendSafeEmergency(openCase.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<OpenCase>() {
                    @Override
                    public void call(OpenCase openCase) {
                        Log.d(TAG, "call: ctr " + openCase.getCaseStatus());
                        onBackPressed();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }
}
