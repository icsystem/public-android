package com.ics.emergencybutton.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.DatabaseUtils;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ics.emergencybutton.EmergencyButtonApplication;
import com.ics.emergencybutton.EmergencyButtonComponent;
import com.ics.emergencybutton.R;
import com.ics.emergencybutton.databinding.ActivityRegistrationBinding;
import com.ics.emergencybutton.internal.CachedAccountStore;
import com.ics.emergencybutton.internal.CachedManager;
import com.ics.emergencybutton.network.EmergencyButtonService;
import com.ics.emergencybutton.pojo.AccountInfo;

import java.util.Random;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegistrationActivity extends Activity {

    private final String TAG = RegistrationActivity.class.getSimpleName();

    private ActivityRegistrationBinding binding;

    @Inject EmergencyButtonService emergencyButtonService;
    @Inject CachedAccountStore cachedAccountStore;
    @Inject CachedManager cachedManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EmergencyButtonApplication.getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_registration);
        initViews();
    }

    private void initViews(){
        binding.btnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(RegistrationActivity.this);
                progressDialog.setMessage("Mohon Tunggu ...");

                String name = binding.nameInput.getText().toString();
                String ktpNumber = binding.noktpInput.getText().toString();
                final String phoneNumber = binding.nohpInput.getText().toString();
                String domisili = binding.domisiliInput.getText().toString();
                String password = binding.passwordInput.getText().toString();
                String confirmPassword = binding.confirmPasswordInput.getText().toString();

                if(name.isEmpty()){
                    Toast.makeText(RegistrationActivity.this, "Mohon isi nama", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(ktpNumber.isEmpty()){
                    Toast.makeText(RegistrationActivity.this, "Mohon isi ktp", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(phoneNumber.isEmpty()){
                    Toast.makeText(RegistrationActivity.this, "Mohon isi no Hp", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(domisili.isEmpty()){
                    Toast.makeText(RegistrationActivity.this, "Mohon isi domisili", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(password.isEmpty()){
                    Toast.makeText(RegistrationActivity.this, "Mohon password ktp", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(confirmPassword.isEmpty()){
                    Toast.makeText(RegistrationActivity.this, "Mohon konfirmasi password ktp", Toast.LENGTH_SHORT).show();
                    return;
                }
                    emergencyButtonService.registration(name,ktpNumber,phoneNumber,domisili,password,confirmPassword)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<AccountInfo>() {
                                @Override
                                public void onStart() {
                                    super.onStart();
                                    progressDialog.show();
                                }

                                @Override
                                public void onCompleted() {
                                    Log.d(TAG, "onCompleted: ctr ");
                                    progressDialog.dismiss();
                                    startActivity(VerificationCodeActivity.generateVerification(RegistrationActivity.this));
                                    finish();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d(TAG, "onError: ctr " + e.getMessage());
                                    e.printStackTrace();
                                    if(e.getMessage().contains("422 Unprocessable Entity")){
                                        Toast.makeText(RegistrationActivity.this, " Pastikan data benar", Toast.LENGTH_SHORT).show();
                                    }
                                    progressDialog.dismiss();
                                }

                                @Override
                                public void onNext(AccountInfo accountInfo) {
                                    Log.d(TAG, "onNext: ctr " + accountInfo.getAuthenticationToken());
                                    cachedAccountStore.cachedAccountInfo(accountInfo);
                                    cachedManager.savePhoneNumber(phoneNumber);
                                }
                            });

            }
        });

    }

    private String randomIddToken(){
        Random r = new Random();
        int randomNumber = r.nextInt(8000 - 1) + 2;
        return "abcdefghijklmnopq" + randomNumber;
    }
}
