package com.ics.emergencybutton.pojo;

/**
 * Created by angga on 10/22/16.
 */

public class OpenCase {
    private String id;
    private String location;
    private String description;
    private double lat;
    private double lng;
    private String caseStatus;
    private CaseType caseType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getCaseStatus() {
        return caseStatus;
    }

    public CaseType getCaseType() {
        return caseType;
    }

    public class CaseType{
        String eventName;
        String title;

        public String getEventName() {
            return eventName;
        }

        public String getTitle() {
            return title;
        }
    }

    @Override
    public String toString() {
        return "OpenCase{" +
                "id='" + id + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", caseStatus='" + caseStatus + '\'' +
                ", caseType=" + caseType +
                '}';
    }
}
