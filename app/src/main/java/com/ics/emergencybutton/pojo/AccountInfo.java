package com.ics.emergencybutton.pojo;

import android.util.Log;

/**
 * Created by angga on 10/19/16.
 */

public class AccountInfo {

    private String authenticationToken;
    private String messages;
    private boolean isVerified;

    public AccountInfo(String authenticationToken,boolean isVerified) {
        this.authenticationToken = authenticationToken;
        this.isVerified = isVerified;
    }

    public String getMessages() {
        return messages;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setVerified(boolean verified) {
        Log.d("ctr ", "setVerified: ctr " + verified);
        isVerified = verified;
    }

    public boolean isVerified() {
        Log.d("ctr ", "isverified: ctr " + isVerified);
        return isVerified;
    }

    @Override
    public String toString() {
        return "AccountInfo{" +
                "authenticationToken='" + authenticationToken + '\'' +
                ", messages='" + messages + '\'' +
                ", isVerified=" + isVerified +
                '}';
    }
}
