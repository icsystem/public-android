package com.ics.emergencybutton.util;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by angga on 10/19/16.
 */

public enum JsonParser {
    INSTANCE;
    private final Gson parser;

    JsonParser() {
        parser = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    public static JsonParser getInstance() {
        return INSTANCE;
    }

    public Gson getParser() {
        return parser;
    }
}
