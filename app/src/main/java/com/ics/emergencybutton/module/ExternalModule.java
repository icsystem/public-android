package com.ics.emergencybutton.module;

import android.content.Context;

import com.ics.emergencybutton.internal.CachedAccountStore;
import com.ics.emergencybutton.internal.CachedManager;
import com.ics.emergencybutton.network.EmergencyButtonService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by angga on 10/16/16.
 */

@Module
public final class ExternalModule {
    @Provides
    @Singleton
    EmergencyButtonService emergencyButtonService(Context context, CachedAccountStore cachedAccountStore,
                                                  CachedManager cachedManager){
        return new EmergencyButtonService(cachedAccountStore,context,cachedManager);
    }
}
