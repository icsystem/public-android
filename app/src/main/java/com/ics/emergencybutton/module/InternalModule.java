package com.ics.emergencybutton.module;

import android.content.Context;

import com.ics.emergencybutton.internal.CachedAccountStore;
import com.ics.emergencybutton.internal.CachedManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by angga on 10/16/16.
 */
@Module
public class InternalModule {
    @Provides
    @Singleton
    CachedAccountStore provideCachedAccountInfo(Context context){
        return new CachedAccountStore(context);
    }
    @Provides
    @Singleton
    CachedManager provideCachedManager(Context context){
        return new CachedManager(context);
    }

}
