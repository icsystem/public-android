package com.ics.emergencybutton.network.response;

import android.util.Log;

import com.ics.emergencybutton.util.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by angga on 10/19/16.
 */

public class Response extends GenericResponse {
    private static final String TAG = Response.class.getSimpleName();

    public <T> T getResult(Class<T> tClass) {
        Log.d(TAG, "getResult: ctr enter response");
        try {
            T result = null;
            JSONObject object = new JSONObject(data.toString());
            Iterator<?> keys = object.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                String s = object.getJSONObject(key).toString();
                Log.d(TAG, "getResult: ctr s key " + s);
                result = JsonParser.getInstance().getParser().fromJson(s, tClass);
            }
            return result;
        } catch (JSONException e) {
            try {
                T result;
                JSONObject object = new JSONObject(data.toString());
                Iterator<?> keys = object.keys();
                String key = (String) keys.next();
                result = (T) object.get(key);
                return result;
            } catch (JSONException e1) {
                if (tClass.equals(Boolean.class)) {
                    return (T) new Boolean(true);
                }
                throw new RuntimeException(e1.getMessage());
            }
        }
    }
}
