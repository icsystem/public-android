package com.ics.emergencybutton.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ics.emergencybutton.BuildConfig;
import com.ics.emergencybutton.internal.CachedAccountStore;
import com.ics.emergencybutton.internal.CachedManager;
import com.ics.emergencybutton.network.response.Response;
import com.ics.emergencybutton.pojo.AccountInfo;
import com.ics.emergencybutton.pojo.OpenCase;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import retrofit.BaseUrl;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by angga on 10/16/16.
 */

public class EmergencyButtonService {
    private final String TAG = EmergencyButtonService.class.getSimpleName();

    private EmergencyButtonApi emergencyButtonApi;
    private CachedAccountStore cachedAccountStore;
    private CachedManager cachedManager;

    private Gson gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create();

    public EmergencyButtonService(final CachedAccountStore cachedAccountStore,final Context context, CachedManager cachedManager) {
        this.cachedAccountStore = cachedAccountStore;
        this.cachedManager = cachedManager;
       final BaseUrl baseUrl = new BaseUrl() {
            @Override
            public HttpUrl url() {

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                String baseUrl = sharedPreferences.getString("api_server", BuildConfig.API_URL);
                Log.d(TAG, " base url: ctr " + baseUrl);
                return HttpUrl.parse(baseUrl);
            }
        };

        // 2. Create and prepare the Client that will be the "backend".
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(120, TimeUnit.SECONDS);
        client.setReadTimeout(120, TimeUnit.SECONDS);
        client.networkInterceptors().add(new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request()
                        .newBuilder();

                if (cachedAccountStore.containAccountInfo()) {
                    String authenticationToken = cachedAccountStore.getCachedAccountInfo().getAuthenticationToken();
//                    builder.addHeader("Authorization", authenticationToken);
                    Log.d(TAG, "BaseUrl: " + baseUrl.url().toString());
                    Log.d(TAG, "Token: " + authenticationToken);
                }

                return chain.proceed(builder.build());
            }
        });
        // 3. Create and prepare the logging interceptor. This is mainly for debugging purpose.
        // TBD: Is it better to use Stetho instead?
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("retrofit", message);
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(loggingInterceptor);

        // 4. Almost done. Now, we can create the Retrofit instance.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();

        // 5. Finally, we can create the model of the API.
        emergencyButtonApi = retrofit.create(EmergencyButtonApi.class);
    }

    private interface EmergencyButtonApi {
        @FormUrlEncoded
        @PUT("/user_societies/session/sign_in")
        Observable<Response> login(@Field("phone_number") String phoneNumber,
                                   @Field("password") String password);

        @FormUrlEncoded
        @POST("/user_societies/session/sign_up")
        Observable<Response> registration(@Field("name") String name,
                                          @Field("ktp_number") String ktpNumber,
                                          @Field("phone_number") String phoneNumber,
                                          @Field("address") String address,
                                          @Field("password") String password,
                                          @Field("password_confirmation") String passwordConfirmation,
                                          @Field("iid_token") String iidToken);

        @FormUrlEncoded
        @PUT("/user_societies/session/confirmation")
        Observable<Response> verification(@Field("activation_code") String activationCode);

        @FormUrlEncoded
        @PUT("/user_societies/session/phone_number")
        Observable<Response> editNumber(@Header("Authorization") String token,
                                        @Field("phone_number") String phoneNumber);

        @FormUrlEncoded
        @PUT("/user_societies/session/resend")
        Observable<Response> resendCode(@Field("phone_number") String phoneNumber);

        @FormUrlEncoded
        @POST("/open_cases/emergency")
        Observable<Response> sendEmergencyCase(@Header("Authorization") String token,
                                           @Field("lat") double lat,
                                           @Field("lng") double lng,
                                           @Field("case_type") String caseType);

        @DELETE("/open_cases/safe")
        Observable<Response> safeEmergency(@Header("Authorization") String token,
                                           @Query("id") String id);
    }

    public Observable<AccountInfo> login(String phoneNumber, String password){
        return emergencyButtonApi.login(phoneNumber,password)
                .map(new Func1<Response, AccountInfo>() {
                    @Override
                    public AccountInfo call(Response response) {
                        Log.d(TAG, "call: " + Thread.currentThread());
                        return response.getResult(AccountInfo.class);
                    }
                });

    }

    public Observable<AccountInfo> registration(String name,String ktpNumber,String phoneNumber,String address,
                                                String password,String passwordConfirmation){

        return emergencyButtonApi.registration(name,ktpNumber,phoneNumber,address,password,passwordConfirmation,
                cachedManager.getFCMToken())
                .map(new Func1<Response, AccountInfo>() {
                    @Override
                    public AccountInfo call(Response response) {
                        return response.getResult(AccountInfo.class);
                    }
                });
    }

    public  Observable<AccountInfo> editNumber(String phoneNumber){
        return emergencyButtonApi.editNumber(cachedAccountStore.getCachedAccountInfo().getAuthenticationToken()
                ,phoneNumber)
                .map(new Func1<Response, AccountInfo>() {
                    @Override
                    public AccountInfo call(Response response) {
                        return response.getResult(AccountInfo.class);
                    }
                });
    }

    public Observable<AccountInfo> verification(String activeCode){
        return emergencyButtonApi.verification(activeCode)
                .map(new Func1<Response, AccountInfo>() {
                    @Override
                    public AccountInfo call(Response response) {
                        return response.getResult(AccountInfo.class);
                    }
                });
    }

    public Observable<AccountInfo> resendCode(String phoneNumber){
        return emergencyButtonApi.resendCode(phoneNumber)
                .map(new Func1<Response, AccountInfo>() {
                    @Override
                    public AccountInfo call(Response response) {
                        return response.getResult(AccountInfo.class);
                    }
                });
    }
    public Observable<OpenCase> sendEmergencyCase(double latitude , double longitude, String caseType){
        return emergencyButtonApi.sendEmergencyCase(cachedAccountStore.getCachedAccountInfo().getAuthenticationToken(),
                latitude,longitude,caseType)
                .map(new Func1<Response, OpenCase>() {
                    @Override
                    public OpenCase call(Response response) {
                        return response.getResult(OpenCase.class);
                    }
                });
    }

    public Observable<OpenCase> sendSafeEmergency(String id){
        return emergencyButtonApi.safeEmergency(cachedAccountStore.getCachedAccountInfo().getAuthenticationToken(),
                id)
                .map(new Func1<Response, OpenCase>() {
                    @Override
                    public OpenCase call(Response response) {
                        return response.getResult(OpenCase.class);
                    }
                });
    }


}
