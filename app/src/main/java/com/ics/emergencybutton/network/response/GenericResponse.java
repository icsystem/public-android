package com.ics.emergencybutton.network.response;

import com.google.gson.JsonObject;

/**
 * Created by angga on 10/19/16.
 */

public class GenericResponse {
    protected JsonObject data;

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "GenericResponse{" +
                ", data=" + data +
                '}';
    }
}

